#Authors: Daniel Ramirez, Ramon Collazo
#Stu No : 801-12-6735, 801-12-1480
#Description: This is a recursive function finds a numbers position on a list
def buscaEnLista(L,item): 
	if L[-1] == item: 
		return len(L) -1 
	elif len(L) -1 <= 0: 
		return -1 
	else: 
		del L[-1] 
	return buscaEnLista(L,item)
print buscaEnLista([2,3,4,10,40], 10)  # imprime 3
print buscaEnLista([2,3,4,10,40], 15)  # imprime -1

