#Authors: Daniel Ramirez, Ramon Collazo
#Stu No : 801-12-6735, 801-12-1480
#Description: This program fills the unconected empty spaces on a simulated graph

# Search for the letter and return
# its position as a tuple
def search(M,letter):
    for i in range(len(M)):
        for j in range(len(M[0])):
            if M[i][j] == letter:
                return (i,j)
    return (-1,-1)

# Given a string representing the maze
# return it as a list of lists.
def splitToListOfLists(st):
    M =  st.split("\n")
    L = []
    for line in M:
        LL = []
        for c in line:
            LL.append(c)
        L.append(LL)
    return L

# prints the list of lists as a string
def printLoLAsString(M):
    st = ""
    for i in M:
        st = st + "".join(i) + "\n"
    print st

def fill(M,pos):
    M[pos[0]][pos[1]] = "P"
    if M[pos[0]+1][pos[1]] == " ":
        fill(M,(pos[0]+1,pos[1]))
    if M[pos[0]-1][pos[1]] == " ":
        fill(M,(pos[0]-1,pos[1]))
    if M[pos[0]][pos[1]+1] == " ":
        fill(M,(pos[0],pos[1]+1))
    if M[pos[0]][pos[1]-1] == " ":
        fill(M,(pos[0],pos[1]-1))
    return

maze1 = "\
WWWWWWWWWWWWWWWWWWWWWWW\n\
W    W          W     W\n\
W               W     W\n\
W P  W          W     W\n\
W    W          W     W\n\
WWWWWWWWWWWWWWWWWWWWWWW"

maze2 = "\
WWWWWWWWWWWWWWWWWWWWWWW\n\
W    W          W     W\n\
W    W          WWWWWWW\n\
W P  WWWWWWW WWWW     W\n\
W        W            W\n\
WWWWWWWWWWWWWWWWWWWWWWW"


M = splitToListOfLists(maze1)

fill(M,search(M,"P"))

printLoLAsString(M)
