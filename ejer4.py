#Authors: Daniel Ramirez, Ramon Collazo
#Stu No : 801-12-6735, 801-12-1480
#Description: This program goes tru a list and displays its contents
def recorre(unaLista):
    if unaLista == None:
    	return
    print unaLista[0]
    unaLista = unaLista[1]
    recorre(unaLista)

LL = (15, (10, (34, (2, None))))
recorre(LL)