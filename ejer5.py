#Authors: Daniel Ramirez, Ramon Collazo
#Stu No : 801-12-6735, 801-12-1480
#Description: This is a recursive function that searches a tuplo for a number

def buscaEnArbol(r, elem): 
    if r[0] == elem or r[1] == elem: 
        return True 
    elif r[1][0] == None and r[1][1] == None: 
        return False 
    if elem > r[0]: 
        r = r[1][1] 
        return buscaEnArbol(r,elem) 
    else: 
        r = r[1][0] 
        return buscaEnArbol(r, elem) 

B = ( 10, ( ( 5, (( 2, (None,None) ), ( 8, (None,None) ) ) ) , ( 24, (None, ( 30, (None,None) )) ) ) ) 
print buscaEnArbol(B, 39)
print buscaEnArbol(B, 2)
