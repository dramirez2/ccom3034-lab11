#Authors: Daniel Ramirez, Ramon Collazo
#Stu No : 801-12-6735, 801-12-1480
#Description: This is a recursive function that calculates a sum of a list's contents
def sumaLista(lst):
	if len(lst) == 1:
		return lst[0]
	return  int(lst[0]) + int(sumaLista(lst[1:]))

print sumaLista([5,4,3,2,1])